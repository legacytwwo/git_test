## 2.0.1 (2024-03-05)

### fix (1 change)

- [Тестовый прикол](legacytwwo/git_test@f2b77bd17755d6c38738b7e41f56e081f25ad761)

## 1.0.4 (2024-03-04)

### fix (2 changes)

- [Тестовый прикол](legacytwwo/git_test@f2b77bd17755d6c38738b7e41f56e081f25ad761)
- [fix(#MDTC-123): Тестовое описание](legacytwwo/git_test@714d6141643151fe39be98c2bc130a6ce74d97c2)

### feat (1 change)

- [fix(#MF-12): Наыва аы](legacytwwo/git_test@7750bd8bd55a509c1825a0f4adb681ec939cdb86)

## 1.0.3 (2024-03-04)

### feat (1 change)

- [fix(#MF-12): Наыва аы](legacytwwo/git_test@7750bd8bd55a509c1825a0f4adb681ec939cdb86)

### fix (1 change)

- [fix(#MDTC-123): Тестовое описание](legacytwwo/git_test@714d6141643151fe39be98c2bc130a6ce74d97c2)

## 1.0.1 (2024-03-04)

No changes.

## 0.0.3 (2024-03-04)

No changes.

## 0.0.2 (2024-03-04)

No changes.
